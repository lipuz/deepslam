# -*- coding: utf-8 -*-
"""
Created on Wed Apr 11 19:50:37 2018

@author: zhoul
"""

from __future__ import absolute_import, division, print_function

import os
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
import tensorflow.contrib.eager as tfe
import PIL.Image as pil
import sys
import utils
from SLAMNet import SLAMNet
#import SLAMLearner
#tf.enable_eager_execution()

def preprocess_image(image):
    # Assuming input image is uint8
    image = tf.image.convert_image_dtype(image, dtype=tf.float32)
    return image * 2. -1.

print("TensorFlow version: {}".format(tf.VERSION))
#print("Eager execution: {}".format(tf.executing_eagerly()))
tf.reset_default_graph()
dir = '../../dataset/kitti/kitti_raw_eigen/2011_09_26_drive_0001_sync_02/0000000001.jpg'
img = pil.open(dir)

I = np.array(img)

x = tf.constant(I[np.newaxis,:,:,:])
x = tf.image.convert_image_dtype(x, dtype=tf.float32)
x = x*2. - 1

s = tf.shape(x)
w = s[1]

batch_size = 1
img_h = 128
img_w = 416
img_c = 3
num_scales = 4
window_size = 3
bilateral = False
pos_net_len = 3
smooth_weight = 0.5
ba_weight = 1
learning_rate = 0.0002
beta1 = 0.9

x = [tf.slice(x, [0,0,img_w*i,0], [-1,-1,img_w,-1]) for i in range(window_size)]

intrinsic = np.array([241.674463,0.,204.168010,0.,246.284868,59.000832,0.,0.,1.])
intrinsic = np.reshape(intrinsic,[3,3])
intrinsics = [tf.constant(intrinsic/2**n, dtype=tf.float32) for n in range(num_scales)]
intrinsics = tf.stack(intrinsics)
intrinsics = tf.expand_dims(intrinsics, axis=0)

ckpt_file = './model/model-190532'
#dataset_dir = '../../dataset/kitti/kitti_raw_eigen/'
dataset_dir = 'D:/MyPaper/DeepSLAM/dataset/kitti/kitti_raw_eigen'

slam_net = SLAMNet( batch_size, img_h, img_w, img_c, num_scales, window_size, pos_net_len, bilateral, dataset_dir, smooth_weight, ba_weight, learning_rate, beta1)
#slam_net.build_train_graph()
slam_net.build_slam_net(x, pos_net_len, window_size)
slam_net.build_loss(x, slam_net.forward_pose, slam_net.depth_seq, intrinsics)
#slam_net.build_train_graph()
saver = tf.train.Saver([var for var in tf.model_variables()]) 
#slam_net.build_data_loader()
#slam_net.build_slam_net(x, pos_net_len, window_size)
sv = tf.train.Supervisor(logdir='./model', \
                         save_summaries_secs=0, \
                         saver=None)
config = tf.ConfigProto()
config.gpu_options.allow_growth = True

with sv.managed_session(config=config) as sess:
    saver.restore(sess, ckpt_file)
#    depth = sess.run(slam_net.pred_depth)
    ba_est_depth = sess.run(slam_net.ba_est_depth[0])
    biliear = sess.run(slam_net.bilinear)
    w_x0 = sess.run(slam_net.w_x0)
    w_x1 = sess.run(slam_net.w_x1)
    w_y0 = sess.run(slam_net.w_y0)
    w_y1 = sess.run(slam_net.w_y1)
#    coord_3d = sess.run(slam_net.point_cloud_seq)
#    coord_2d = sess.run(slam_net.proj_pixel_coords)
#    bilinear_maps = sess.run(slam_net.bilinear_maps)
#    loss = sess.run(slam_net.total_loss)
#    mask = sess.run(slam_net.mask)
#    print(est_depth)
    
#mask_1 = coord_2d[0][0,0,:]>0 & (coord_2d[0][0,0,:]<img_w-1) & (coord_2d[0][0,1,:] > 0) &  (coord_2d[0][0,1,:]< img_h-1)

#slam_net.build_train_graph()
#slam_net.build_slam_net(x, pos_len, window_size)
#with tf.Session() as sess:    
#    slam_net.build_slam_net(x,window_size)
#saver = tf.train.Saver([var for var in tf.model_variables()]) 

#with tf.Session() as sess:
##    sess.run(tf.global_variables_initializer())
#    saver.restore(sess, ckpt_file)
#    img = sess.run(slam_net.img_seq)[0][0]
#    pred_depth = sess.run(slam_net.pred_depth[0][0])
#    total_loss = sess.run(slam_net.total_loss)
#    for i, (f_pose, b_pose) in enumerate( zip(slam_net.forward_pose, slam_net.backward_pose)):
##        print(pose)        
#        tt = sess.run(f_pose[0])
#        print(type(tt))
#        print(tt)
#        print('pose {0}: {1}'.format(i, sess.run(f_pose[0])*sess.run(b_pose[0])))
        

#depth = utils.normalize_depth_for_display(pred_depth[0,:,:,0])
plt.figure()
plt.imshow(img)
#plt.imshow(I[:, 0:img_w, : ])
#plt.figure()
#plt.imshow(depth)
plt.show()
