# -*- coding: utf-8 -*-
"""
Created on Wed Apr 11 21:03:41 2018

@author: zhoul
"""
import tensorflow as tf
import numpy as np
import nets
from data_loader import DataLoader
import utils
import tensorflow.contrib.slim as slim

class SLAMNet:
    def __init__(self, batch_size, img_h, img_w, img_c, num_scales, \
                 seq_len, pose_net_len, bilateral, dataset_dir, \
                 smooth_weight, ba_weight, SSIM_weight, gradient_weight,\
                 learning_rate, beta1):
        
        self.batch_size = batch_size
        self.img_h = img_h
        self.img_w = img_w
        self.img_c = img_c    
        self.num_scales = num_scales
        self.dataset_dir = dataset_dir
        self.seq_len = seq_len
        self.bilateral = bilateral
        self.pose_net_len = pose_net_len       
        self.smooth_weight = smooth_weight
        
        self.learning_rate = learning_rate
        self.beta1 = beta1
        
        self.total_loss = 0
        self.pixel_loss = 0
        self.smooth_loss = 0
        self.ba_loss = 0
        self.SSIM_loss = 0
        self.gradient_loss = 0
        
        self.ba_weight = ba_weight
        self.SSIM_weight = SSIM_weight
        self.gradient_weight = gradient_weight
        
        
        self.forward_pose = []
        self.backward_pose = []
        self.depth_seq = [] 
        
        self.tgt_image_all = []
        self.src_image_all = []
        self.proj_image_all = []
        self.proj_error_all = []
        
        self.tgt_gradient_all = []
        self.src_gradient_all = []
        self.proj_gradient_all = []
        self.proj_gradient_error_all = []
        
        self.pixel_mask = []    
        
        self.ba_est_depth = []
        self.ba_depth_error = []
        self.ba_depth_mask = []
        
    def build_train_graph(self):        
        self.build_data_loader()
        self.build_slam_net(self.img_seq, self.pose_net_len, self.seq_len)
        self.build_loss(self.bilateral)
        
        with tf.name_scope("train_op"):
            optim = tf.train.AdamOptimizer(self.learning_rate, self.beta1)
            # self.grads_and_vars = optim.compute_gradients(total_loss, 
            #                                               var_list=train_vars)
            # self.train_op = optim.apply_gradients(self.grads_and_vars)
            self.train_op = slim.learning.create_train_op(self.total_loss, optim)
            self.global_step = tf.Variable(0, 
                                           name='global_step', 
                                           trainable=False)
            self.incr_global_step = tf.assign(self.global_step, 
                                              self.global_step+1)     
    
    def build_data_loader(self):
        self.img_seq = []
        self.depth_seq = []
        self.intrinsics = tf.placeholder(dtype=tf.float32, shape=(self.batch_size, self.num_scales, 3, 3))
        for i in range(self.seq_len):
            img_seq_i = tf.placeholder(dtype=tf.float32, shape=(self.batch_size, self.img_h ,self.img_w, self.img_c))
            depth_seq_i = tf.placeholder(dtype=tf.float32, shape=(self.batch_size, self.img_h ,self.img_w, self.img_c))           
            self.img_seq.append(img_seq_i)
            self.depth_seq.append(depth_seq_i)
            
            
#    def build_data_loader(self):
#        loader = DataLoader(self.dataset_dir,
#                            self.batch_size,
#                            self.img_h,
#                            self.img_w,
#                            self.window_size, 
#                            self.num_scales)
#        
#        with tf.name_scope("data_loading"):
#            img_seq, intrinsics = loader.load_train_batch()
#            img_seq = utils.preprocess_image(img_seq)
#            img_seq = [tf.slice(img_seq, [0,0,0,i*3],[-1,-1,-1,3]) for i in range(img_seq.get_shape()[3].value // 3)]
#            self.img_seq = img_seq
#            self.intrinsics = intrinsics
#        
#        self.steps_per_epoch = loader.steps_per_epoch
        
    def inv_pose(self, pose):
        
#        batch_size = pose.get_shape()[0].value
#        filler = tf.constant([0,0,0,1], dtype='float32', shape=[1, 4])        
#        inv_poses = []
#        for i in range(batch_size):
#            R_i = pose[i, 0:3,0:3]
#            inv_R_i =  tf.transpose(R_i)
#            t_i     = -tf.matmul(inv_R_i, pose[i, 0:3,3:4])
#            inv_pose_i = tf.concat([inv_R_i, t_i], axis = 1)
#            inv_pose_i = tf.concat([inv_pose_i,filler], axis = 0)
#            inv_poses.append(inv_pose_i)
#        inv_poses = tf.stack(inv_poses, axis = 0)
        batch_size = pose.get_shape()[0].value
        filler = tf.constant([0,0,0,1], dtype='float32', shape=[1, 1, 4]) 
        filler = tf.tile(filler, [batch_size, 1, 1])
        inv_R = tf.transpose(pose[:,0:3,0:3], [0,2,1])
        inv_t = -tf.matmul(inv_R, pose[:,0:3,3:4])
        inv_pose = tf.concat([inv_R, inv_t], axis=2)
        inv_pose = tf.concat([inv_pose, filler], axis=1)
        
        return inv_pose
    
    def decouple_pose(self, pose, pose_1):
        ''' pose = pose_1 * pose_2
            return pose_2, given pose and pose_1
        '''

        inv_pose_1 = self.inv_pose(pose_1)        
        pose_2 = tf.matmul(pose*inv_pose_1)

        return pose_2
    
    def serialize_pose(self, poses):        
        pose_cnt = len(poses)
        for j in range(pose_cnt):     
            if j in [pose_cnt//2-1, pose_cnt//2]:
                continue
                
            if j < pose_cnt//2-1:
                poses[j] = self.decouple_pose(poses[j], poses[j+1])
                continue
                
            
            if j > pose_cnt//2:
                poses[j] = self.decouple_pose(poses[j], poses[j-1])
                continue
        
        for j in range(pose_cnt):
            if j < pose_cnt//2:                        
                self.forward_pose.append(self.inv_pose(poses[j]))
                self.backward_pose.append(poses[j])
            else:
                self.forward_pose.append(poses[j])
                self.backward_pose.append(self.inv_pose(poses[j]))       
     
    def get_pose_net_input_item(self, sequences, i):
        
        start = i*(self.pose_net_len-1)
        end = (i+1)*(self.pose_net_len-1)
        tgt_idx =  self.pose_net_len // 2 
        
        tgt_image = sequences[start+tgt_idx]
        
        src_image_stack = sequences[start:end+1]        
        src_image_stack.remove(tgt_image)
        src_image_stack = tf.concat(src_image_stack, axis = 3)
        
        return tgt_image, src_image_stack
        
    
    
    def build_slam_net(self, sequences, pose_net_len, window_size, bilateral = False):
        ''' window_size = (pos_len-1)*(n-1)+pos_len
        '''
        self.depth_seq = []      
        with tf.name_scope("slam_net"):
            n = int((window_size - pose_net_len) // (pose_net_len-1) + 1)
            print('Calcuate pose {0} times'.format(n))
            
            with tf.name_scope("pose_prediction") as pose_sc:
                for i in range(n):
                    pose_mat_i = []
                    is_reuse = True if i>0 else False

                    tgt_image, src_image_stack = self.get_pose_net_input_item(sequences, i)                    
                    
                    pred_poses, _, _ = nets.pose_exp_net(tgt_image, src_image_stack, do_exp=False, is_training=False, is_reuse=is_reuse)
                    pose_cnt = int (pred_poses.get_shape()[1].value)
                    
                    for j in range(pose_cnt):
                        pose_ij = utils.pose_vec2mat(pred_poses[:,j,:])
                        pose_mat_i.append(pose_ij)
                    
                    self.serialize_pose(pose_mat_i)             
                
            with tf.name_scope("depth_prediction") as depth_sc:
                for i in range(window_size):    
                    
                    is_reuse = True if i>0 else False
                          
                    img_i = sequences[i]
                    pred_disp, depth_net_endpoints = nets.disp_net( img_i, is_training=False, is_reuse=is_reuse)    
                    
                    pred_depth = [tf.squeeze(1./disp, axis=3 ) for disp in pred_disp]
                    self.depth_seq.append(pred_depth)


    
    def get_projection(self, intrinsics):
        filler = tf.constant([0.0, 0.0, 0.0, 1.0], shape=[1, 1, 4])
        filler = tf.tile(filler, [self.batch_size, 1, 1])
        intrinsics = tf.concat([intrinsics, tf.zeros([self.batch_size, 3, 1])], axis=2)
        intrinsics = tf.concat([intrinsics, filler], axis=1)
        # Get a 4x4 transformation matrix from 'target' camera frame to 'source'
        # pixel frame.
        return intrinsics
    
    
    def reshape2image(self, x, img_w, img_h):
        N = img_w*img_h
        img_cnt = x.get_shape()[1].value // N
        
        if img_cnt == 1: 
            return tf.reshape(x,[-1,img_h, img_w])
            
        imgs = []
        for i in range(img_cnt):
            x_i = x[:,i*N:(i+1)*N]
            img_i = tf.reshape(x_i,[-1,img_h, img_w])            
            imgs.append(img_i)        
        imgs = tf.concat(imgs, axis=2)
        return imgs
         
    
            
    def build_bundle_adjustment_scale_loss(self, pose_seq, depth_seq, intrinsics):
        batch, height, width  = depth_seq[0].get_shape().as_list()        
        n = len(depth_seq)
        pixel_coords = utils.meshgrid(batch, height, width)
        point_cloud_seq = [tf.reshape(utils.pixel2cam(depth_seq[i], pixel_coords, intrinsics[i]), [batch, -1, height*width]) for i in range(n)]
        
        accum_cloud_seq = [point_cloud_seq[0]]
        for i in range(1,n-1):            
            trans_pc = tf.matmul(pose_seq[i-1], accum_cloud_seq[i-1])
            accum_pc = tf.concat([point_cloud_seq[i], trans_pc], axis = 2)            
            accum_cloud_seq.append(accum_pc)
            
        loss = 0
        img_depth_scale = []
        depth_error_scale = []
        mask_scale = []        

        for i in range(1,n):
            trans_pc_i_1 = tf.matmul(pose_seq[i-1], accum_cloud_seq[i-1])
            z = tf.abs(trans_pc_i_1[:,2,:])
            
            proj = self.get_projection(intrinsics[i-1])
            proj_pixel_coords = utils.cam2pixel(trans_pc_i_1, proj, do_reshape=False)
            z_est, mask = utils.depth_bilinear_sampler(depth_seq[i], proj_pixel_coords) 
            
#            err = tf.abs((z - z_est)/(z+z_est+1e-6))*tf.cast(mask,dtype=tf.float32)    
            err = tf.abs(z-z_est)*tf.cast(mask,dtype=tf.float32) 
            loss += tf.reduce_mean(err)
            
            img_depth_scale.append(self.reshape2image(z_est, width, height))
            
            depth_error_scale.append(self.reshape2image(err, width, height))
            
            mask_scale.append(self.reshape2image(mask, width, height))
            
        return loss, img_depth_scale, depth_error_scale, mask_scale
    
    
    def build_bundle_adjustment_loss(self, pose_seq, depth_seq, intrinsics):
        
        n = len(depth_seq)
        scale_n = len(depth_seq[0])
        
                
        with tf.name_scope("compute_ba_loss"):
            for s in range(scale_n):
                depth_seq_s = []
                intrinsics_s = []
                for i in range(n):           
                    depth_seq_s.append(depth_seq[i][s])
                    intrinsics_s.append(intrinsics[:,s,:,:])   
                ba_loss_s, img_depth_s, depth_error_s, mask_s = self.build_bundle_adjustment_scale_loss(pose_seq, depth_seq_s, intrinsics_s)
                self.ba_loss += ba_loss_s 
                
                self.ba_est_depth.append(img_depth_s)
                self.ba_depth_error.append(depth_error_s)
                self.ba_depth_mask.append(mask_s)
        return self.ba_loss  
    
    def image_reconstruction(self, img_seq, pose_seq, depth_seq, intrinsics):        
        with tf.name_scope("image_reconstruction"):   
            
            for i in range(len(img_seq) - 1):  
                tgt_image_i = []
                src_image_i = []
                proj_image_i = []
                
                src_gradient_i = []
                tgt_gradient_i = []
                proj_gradient_i = []
                
                pixel_mask_i = []
                
                
                tgt_image = img_seq[i]
                src_image = img_seq[i+1]
                pred_depth = depth_seq[i]
                
                for s in range(len(pred_depth)):                   
                    curr_tgt_image = tf.image.resize_area(tgt_image, [int(self.img_h/(2**s)), int(self.img_w/(2**s))]) 
                    curr_src_image = tf.image.resize_area(src_image, [int(self.img_h/(2**s)), int(self.img_w/(2**s))])                     
                    curr_proj_image, mask = utils.projective_inverse_warp(curr_src_image, pred_depth[s], pose_seq[i], intrinsics[:,s,:,:])  
                    
                    curr_tgt_gradient = self.gradient(curr_tgt_image)
                    curr_src_gradient = self.gradient(curr_src_image)
                    curr_proj_gradient, _ = utils.projective_inverse_warp(curr_src_gradient, pred_depth[s][:,:-1,:-1], pose_seq[i], intrinsics[:,s,:,:] )                   
                    
                    tgt_image_i.append(curr_tgt_image)
                    src_image_i.append(curr_src_image)
                    proj_image_i.append(curr_proj_image)                    
                    
                    src_gradient_i.append(curr_src_gradient)
                    tgt_gradient_i.append(curr_tgt_gradient)
                    proj_gradient_i.append(curr_proj_gradient)
                    
                    pixel_mask_i.append(tf.cast(mask, tf.float32))
                
                self.tgt_image_all.append(tgt_image_i)
                self.src_image_all.append(src_image_i)
                self.proj_image_all.append(proj_image_i)
                
                self.tgt_gradient_all.append(tgt_gradient_i)
                self.src_gradient_all.append(src_gradient_i)
                self.proj_gradient_all.append(proj_gradient_i)
                
                
                self.pixel_mask.append(pixel_mask_i)
                    
        
   
    def build_SSIM_loss(self, predict_image_all, tgt_image_all, pixel_mask):
        with tf.name_scope("compute_SSIM_loss"):
            for i in range(self.window_size - 1):
                for s in range(self.num_scales): 
                    current_SSIM_loss = self.SSIM(predict_image_all[i][s], tgt_image_all[i][s])
                    h = current_SSIM_loss.get_shape()[1].value
                    w = current_SSIM_loss.get_shape()[2].value
                    current_SSIM_loss = current_SSIM_loss*tf.slice(pixel_mask[i][s], [0, 1, 1, 0], [-1, h, w, -1])
                    self.SSIM_loss += tf.reduce_mean(current_SSIM_loss)
            
        return self.SSIM_loss
        
     
    def build_gradient_loss(self, predict_gradient_all, tgt_gradient_all, pixel_mask):
        with tf.name_scope('compute_gradient_loss'):
            for i in range(self.window_size - 1): 
                proj_error_i = []
                for s in range(self.num_scales):
                    curr_proj_error = tf.abs(predict_gradient_all[i][s] - tgt_gradient_all[i][s])*pixel_mask[i][s][:,:-1,:-1,:]
                    self.gradient_loss += (tf.reduce_mean(curr_proj_error) / 2**s)
                    
                    proj_error_i.append(curr_proj_error)
                self.proj_gradient_error_all.append(proj_error_i)
            return self.gradient_loss
        
    def build_pixel_loss(self, predict_image_all, tgt_image_all, pixel_mask):       
        with tf.name_scope("compute_pixel_loss"): 
            for i in range(self.window_size - 1):            
                proj_error_i = []                    
                for s in range(self.num_scales):  
                    curr_proj_error = tf.abs(predict_image_all[i][s] - tgt_image_all[i][s])*pixel_mask[i][s]
                    self.pixel_loss += tf.reduce_mean(curr_proj_error)
                    
                    proj_error_i.append(curr_proj_error)
                self.proj_error_all.append(proj_error_i)
                
        return self.pixel_loss
    
    
    def build_smooth_loss(self, depth_seq, img_seq):        
        with tf.name_scope("compute_smooth_loss"):
            for i in range(self.window_size - 1):
                for s in range(self.num_scales): 
                    depth = tf.expand_dims(depth_seq[i][s], axis=3)
                    curr_smooth_loss = self.compute_smooth_loss(depth, img_seq[i][s]) 
                    self.smooth_loss += (curr_smooth_loss / (2**s))
        return self.smooth_loss
    
    
    def build_loss(self, bilateral=False):
        with tf.name_scope("comput_loss"):
            self.image_reconstruction(self.img_seq, self.forward_pose, self.depth_seq, self.intrinsics)
            self.image_pyramid = self.tgt_image_all[:]
            self.image_pyramid.append(self.src_image_all[-1])
            
            self.build_bundle_adjustment_loss(self.forward_pose, self.depth_seq, self.intrinsics)
            self.build_pixel_loss(self.proj_image_all, self.tgt_image_all, self.pixel_mask)
#            self.build_gradient_loss(self.proj_gradient_all, self.tgt_gradient_all, self.pixel_mask)
            self.build_SSIM_loss(self.proj_image_all, self.tgt_image_all, self.pixel_mask)
            self.build_smooth_loss(self.depth_seq, self.image_pyramid)
            
            if bilateral:
                reverse_img_seq = self.img_seq[::-1]
                reverse_depth_seq = self.depth_seq[::-1]
                reverse_pose = self.backward_pose[::-1]               
                self.image_reconstruction(reverse_img_seq, reverse_pose, reverse_depth_seq, self.intrinsics)
                start = len(self.proj_image_all)//2
                
                self.build_bundle_adjustment_loss(reverse_pose, reverse_depth_seq, self.intrinsics)                
                self.build_pixel_loss(self.proj_image_all[start:], self.tgt_image_all[start:], self.pixel_mask[start:])
#                self.build_gradient_loss(self.proj_gradient_all[start:], self.tgt_gradient_all[start:], self.pixel_mask[start:])
                self.build_SSIM_loss(self.proj_image_all[start:], self.tgt_image_all[start:], self.pixel_mask[start:])
                
                
            self.total_loss = (1-self.SSIM_weight)*self.pixel_loss + \
                              self.smooth_weight*self.smooth_loss + \
                              self.ba_weight*self.ba_loss  + \
                              self.SSIM_weight*self.SSIM_loss # + \
#                              self.gradient_weight*self.gradient_loss
        return self.total_loss
    
        
        
                   
#    def build_loss(self, img_seq, pose_seq, depth_seq, intrinsics):
#        with tf.name_scope("compute_loss"):
#
#            tgt_image_all = []
#            src_image_all = []
#            proj_image_all = []
#            proj_error_all = []
#            
#            for i in range(len(img_seq) - 1):                
#                tgt_image = img_seq[i]
#                src_image = img_seq[i+1]
#                pred_depth = depth_seq[i]
#
#                for s in range(len(pred_depth)):
#                    
#                    if self.smooth_weight > 0:
#                        self.smooth_loss += self.smooth_weight/(2**s) * self.compute_smooth_loss(pred_depth[s])
#                    
#                    curr_tgt_image = tf.image.resize_area(tgt_image, [int(self.img_h/(2**s)), int(self.img_w/(2**s))]) 
#                    curr_src_image = tf.image.resize_area(src_image, [int(self.img_h/(2**s)), int(self.img_w/(2**s))])                    
#                    curr_proj_image = utils.projective_inverse_warp(curr_src_image, tf.squeeze(pred_depth[s], axis=3), \
#                                                                    pose_seq[i], intrinsics[:,s,:,:])                    
#                    curr_proj_error = tf.abs(curr_proj_image - curr_tgt_image)                    
#                    self.pixel_loss += tf.reduce_mean(curr_proj_error) 
#                    
#                    tgt_image_all.append(curr_tgt_image)
#                    src_image_all.append(curr_src_image)
#                    proj_image_all.append(curr_proj_image)
#                    proj_error_all.append(curr_proj_error)
#                    
#            self.build_bundle_adjustment_loss(pose_seq, depth_seq, intrinsics)
#            self.total_loss = self.total_loss + self.pixel_loss + self.smooth_loss + self.ba_loss     
#            
#        return self.total_loss
        

    def gradient_x(self, img):
        gx = img[:,:,:-1,:] - img[:,:,1:,:]
        return gx

    def gradient_y(self, img):
        gy = img[:,:-1,:,:] - img[:,1:,:,:]        
        return gy
    
    def gradient(self, img):
        image_gradients_x = tf.abs(self.gradient_x(img))
        image_gradients_y = tf.abs(self.gradient_y(img))
        
        if img.get_shape()[3].value > 1:
            image_gradients_x = tf.reduce_mean(image_gradients_x, 3, keepdims=True)
            image_gradients_y = tf.reduce_mean(image_gradients_y, 3, keepdims=True)
        
        h = image_gradients_x.get_shape()[1].value
        w = image_gradients_x.get_shape()[2].value
        
        image_gradients_x = tf.slice(image_gradients_x, [0,0,0,0], [-1, h-1,-1,-1])
        image_gradients_y = tf.slice(image_gradients_y, [0,0,0,0], [-1,-1, w, -1])
        
        return image_gradients_x + image_gradients_y
        
        
                    
    def SSIM(self, x, y):
        C1 = 0.01 ** 2
        C2 = 0.03 ** 2

        mu_x = slim.avg_pool2d(x, 3, 1, 'VALID')
        mu_y = slim.avg_pool2d(y, 3, 1, 'VALID')

        sigma_x  = slim.avg_pool2d(x ** 2, 3, 1, 'VALID') - mu_x ** 2
        sigma_y  = slim.avg_pool2d(y ** 2, 3, 1, 'VALID') - mu_y ** 2
        sigma_xy = slim.avg_pool2d(x * y , 3, 1, 'VALID') - mu_x * mu_y

        SSIM_n = (2 * mu_x * mu_y + C1) * (2 * sigma_xy + C2)
        SSIM_d = (mu_x ** 2 + mu_y ** 2 + C1) * (sigma_x + sigma_y + C2)

        SSIM = SSIM_n / SSIM_d

        return tf.clip_by_value((1 - SSIM) / 2, 0, 1) 
    

    def compute_smooth_loss(self, depth, img):        
                            
        disp_gradients_x = self.gradient_x(depth)
        disp_gradients_y = self.gradient_y(depth)

        image_gradients_x = self.gradient_x(img)
        image_gradients_y = self.gradient_y(img)

        weights_x = tf.exp(-tf.reduce_mean(tf.abs(image_gradients_x), 3, keepdims=True)) 
        weights_y = tf.exp(-tf.reduce_mean(tf.abs(image_gradients_y), 3, keepdims=True)) 

        smoothness_x = disp_gradients_x * weights_x
        smoothness_y = disp_gradients_y * weights_y
        
        smoothness_x_loss = tf.reduce_mean(tf.abs(smoothness_x))
        smoothness_y_loss = tf.reduce_mean(tf.abs(smoothness_y))
        smoothness_loss  = smoothness_x_loss + smoothness_y_loss
        return smoothness_loss    

#    def compute_smooth_loss(self, pred_disp):
#        def gradient(pred):
#            D_dy = pred[:, 1:, :, :] - pred[:, :-1, :, :]
#            D_dx = pred[:, :, 1:, :] - pred[:, :, :-1, :]
#            return D_dx, D_dy
#        dx, dy = gradient(pred_disp)
#        dx2, dxdy = gradient(dx)
#        dydx, dy2 = gradient(dy)
#        return tf.reduce_mean(tf.abs(dx2)) + \
#               tf.reduce_mean(tf.abs(dxdy)) + \
#               tf.reduce_mean(tf.abs(dydx)) + \
#               tf.reduce_mean(tf.abs(dy2))
                        
                        
              
                        
                    
                        
            
        
        
        
