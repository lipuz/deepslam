from __future__ import division
import os
import time
import math
import numpy as np
import tensorflow as tf
import tensorflow.contrib.slim as slim
from data_loader import DataLoader
from nets import *
from utils import *
from SLAMNet import *

class SLAMLearner(object):
    def __init__(self):
        pass           

    def train(self, opt):
        
        slam_net = SLAMNet(opt.batch_size, opt.img_height, opt.img_width, opt.img_c, opt.num_scales, \
                           opt.window_size, opt.pose_net_len, opt.bilateral, opt.dataset_dir, \
                           opt.smooth_weight, opt.ba_weight, opt.SSIM_weight, opt.gradient_weight,\
                           opt.learning_rate, opt.beta1)
        
        slam_net.build_train_graph()
        
        steps_per_epoch = slam_net.steps_per_epoch
        
        self.collect_summaries(slam_net)      

        with tf.name_scope("parameter_count"):
            self.parameter_count = tf.reduce_sum([tf.reduce_prod(tf.shape(v)) for v in tf.trainable_variables()])        
        
        self.saver = tf.train.Saver([var for var in tf.model_variables()] + \
                                    [slam_net.global_step],
                                     max_to_keep=10)           
        
        sv = tf.train.Supervisor(logdir=os.path.join(opt.checkpoint_dir, 'tensorboard'), 
                                 save_summaries_secs=0, 
                                 saver=None)
        
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        
        with sv.managed_session(config=config) as sess:
            self.print_net_info(sess)         
            self.restore_net(opt, sess)
            
            start_time = time.time()
            for step in range(1, opt.max_steps):
                fetches = {
                    "train": slam_net.train_op,
                    "global_step": slam_net.global_step,
                    "incr_global_step": slam_net.incr_global_step
                }

                if step % opt.summary_freq == 0:
                    fetches["loss"] = slam_net.total_loss
                    fetches["summary"] = sv.summary_op

                results = sess.run(fetches)
                gs = results["global_step"]

                if step % opt.summary_freq == 0:
                    sv.summary_writer.add_summary(results["summary"], gs)
                    train_epoch = math.ceil(gs / steps_per_epoch)
                    train_step = gs - (train_epoch - 1) * steps_per_epoch
                    print("Epoch: [%2d] [%5d/%5d] time: %4.4f/it loss: %.3f" \
                          % (train_epoch, train_step, steps_per_epoch, \
                             (time.time() - start_time)/opt.summary_freq, \
                             results["loss"]))
                    start_time = time.time()

                if step % opt.save_latest_freq == 0:
                    self.save(sess, opt.checkpoint_dir, 'latest')

                if step % steps_per_epoch == 0:
                    self.save(sess, opt.checkpoint_dir, gs)
                    
    def collect_summaries(self, slam_net):

        tf.summary.scalar("total_loss", slam_net.total_loss)
        tf.summary.scalar("pixel_loss", slam_net.pixel_loss)
        tf.summary.scalar("smooth_loss", slam_net.smooth_loss)
        tf.summary.scalar('SSIM_loss', slam_net.SSIM_loss)
        tf.summary.scalar("ba_loss", slam_net.ba_loss)
#        tf.summary.scalar("gradient_loss", slam_net.gradient_loss)
        
        scale_num = [0]
        
        for n in range(slam_net.window_size):
            pred_depth = slam_net.depth_seq[n]
            for i, s in enumerate(scale_num):
                tf.summary.image("step%d_scale%d_depth" % (n, i), tf.expand_dims(pred_depth[s], axis=3))
                tf.summary.image('step%d_scale%d_disparity_image' % (n, i), tf.expand_dims(1./pred_depth[s], axis=3))
        
        image_cnt = len(slam_net.tgt_image_all)
        forward_image_cnt = image_cnt//2 if slam_net.bilateral else image_cnt
            
        for n in range(forward_image_cnt):  
            for i, s in enumerate(scale_num):
                tf.summary.image('forward_step%d_scale%d_target_image' % (n, i), deprocess_image(slam_net.tgt_image_all[n][s]))
                tf.summary.image('forward_step%d_scale%d_src_image' % (n, i), deprocess_image(slam_net.src_image_all[n][s]))
                tf.summary.image('forward_step%d_scale%d_projected_image' % (n, i), deprocess_image(slam_net.proj_image_all[n][s]))
                tf.summary.image('forward_step%d_scale%d_proj_error_image'% (n, i), deprocess_image(slam_net.proj_error_all[n][s]))
                
#                tf.summary.image('forward_step%d_scale%d_target_gradient' % (n, i), slam_net.tgt_gradient_all[n][s])
#                tf.summary.image('forward_step%d_scale%d_src_gradient' % (n, i), slam_net.src_gradient_all[n][s])
#                tf.summary.image('forward_step%d_scale%d_projected_gradient' % (n, i), slam_net.proj_gradient_all[n][s])
#                tf.summary.image('forward_step%d_scale%d_proj_error_gradient'% (n, i), slam_net.proj_gradient_error_all[n][s])
                
                tf.summary.image('forward_step%d_scale%d_mask' % (n, i), tf.cast(slam_net.pixel_mask[n][s], dtype = tf.float32))                
        
        for i, s in enumerate(scale_num):   
#        for s in range(len(slam_net.ba_est_depth)):
            for n in range(len(slam_net.ba_est_depth[s])):
                tf.summary.image('forward_ba_step%d_scale%d_depth' % (n, i), tf.cast(tf.expand_dims(slam_net.ba_est_depth[s][n], axis=3), dtype=tf.float32))
                tf.summary.image('forward_ba_step%d_scale%d_depth_error' % (n, i), tf.cast(tf.expand_dims(slam_net.ba_depth_error[s][n], axis=3), dtype = tf.float32))
                tf.summary.image('forward_ba_step%d_scale%d_mask' % (n, i), tf.cast(tf.expand_dims(slam_net.ba_depth_mask[s][n], axis=3), dtype = tf.float32))
                
        if slam_net.bilateral:
            for n in range(forward_image_cnt,image_cnt): 
                for i, s in enumerate(scale_num):
                    tf.summary.image('backward_step%d_scale%d_target_image' % (n, i), deprocess_image(slam_net.tgt_image_all[n][s]))
                    tf.summary.image('backward_step%d_scale%d_src_image' % (n, i), deprocess_image(slam_net.src_image_all[n][s]))
                    tf.summary.image('backward_step%d_scale%d_projected_image' % (n, i), deprocess_image(slam_net.proj_image_all[n][s]))
                    tf.summary.image('backward_step%d_scale%d_proj_error_image'% (n, i), deprocess_image(slam_net.proj_error_all[n][s]))
                    
#                    tf.summary.image('backward_step%d_scale%d_target_gradient' % (n, i), slam_net.tgt_gradient_all[n][s])
#                    tf.summary.image('backward_step%d_scale%d_src_gradient' % (n, i), slam_net.src_gradient_all[n][s])
#                    tf.summary.image('backward_step%d_scale%d_projected_gradient' % (n, i), slam_net.proj_gradient_all[n][s])
#                    tf.summary.image('backward_step%d_scale%d_proj_error_gradient'% (n, i), slam_net.proj_gradient_error_all[n][s])
                    
                    tf.summary.image('backward_step%d_scale%d_mask' % (n, i), tf.cast(slam_net.pixel_mask[n][s], dtype = tf.float32)) 
        
            scale_num = [len(slam_net.ba_est_depth) // 2]
            for i, s in enumerate(scale_num):   
                for n in range(len(slam_net.ba_est_depth[s])):
                    tf.summary.image('backward_ba_step%d_scale%d_depth' % (n, i), tf.cast(tf.expand_dims(slam_net.ba_est_depth[s][n], axis=3), dtype=tf.float32))
                    tf.summary.image('backward_ba_step%d_scale%d_depth_error' % (n, i), tf.cast(tf.expand_dims(slam_net.ba_depth_error[s][n], axis=3), dtype = tf.float32))
                    tf.summary.image('bacward_ba_step%d_scale%d_mask' % (n, i), tf.cast(tf.expand_dims(slam_net.ba_depth_mask[s][n], axis=3), dtype = tf.float32))
                    
            

                
        
        for n in range(len(slam_net.forward_pose)):
            tf.summary.histogram("step%d_tx" % n, slam_net.forward_pose[n][:,:,0])
            tf.summary.histogram("step%d_ty" % n, slam_net.forward_pose[n][:,:,1])
            tf.summary.histogram("step%d_tz" % n, slam_net.forward_pose[n][:,:,2])
            
#            tf.summary.histogram("step%d_rx" % n, slam_net.forward_pose[n][:,:,3])
#            tf.summary.histogram("step%d_ry" % n, slam_net.forward_pose[n][:,:,4])
#            tf.summary.histogram("step%d_rz" % n, slam_net.forward_pose[n][:,:,5])
        # for var in tf.trainable_variables():
        #     tf.summary.histogram(var.op.name + "/values", var)
        # for grad, var in self.grads_and_vars:
        #     tf.summary.histogram(var.op.name + "/gradients", grad)
        
    def print_net_info(self, sess):           
        print('Trainable variables: ')
        for var in tf.trainable_variables():
            print(var.name)
            
        print("parameter_count =", sess.run(self.parameter_count))        
     
    def restore_net(self, opt, sess):
        if opt.continue_train:
            if opt.init_checkpoint_file is None:
                checkpoint = tf.train.latest_checkpoint(opt.checkpoint_dir)
            else:
                checkpoint = opt.init_checkpoint_file
            print("Resume training from previous checkpoint: %s" % checkpoint)
            self.saver.restore(sess, checkpoint)
    
    def save(self, sess, checkpoint_dir, step):
        model_name = 'model'
        print(" [*] Saving checkpoint to %s..." % checkpoint_dir)
        if step == 'latest':
            if(os.path.isfile(os.path.join(checkpoint_dir, 'model.latest.data-00000-of-00001'))):
                os.remove(os.path.join(checkpoint_dir, 'model.latest.data-00000-of-00001'))
                os.remove(os.path.join(checkpoint_dir, 'model.latest.index'))
                os.remove(os.path.join(checkpoint_dir, 'model.latest.meta'))
            self.saver.save(sess, 
                            os.path.join(checkpoint_dir, model_name + '.latest'))
        else:
            self.saver.save(sess, 
                            os.path.join(checkpoint_dir, model_name),
                            global_step=step)
